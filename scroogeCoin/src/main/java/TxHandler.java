import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class TxHandler {

	
	private UTXOPool _utxoPool;

	/**
	 * Creates a public ledger whose current UTXOPool (collection of unspent
	 * transaction outputs) is {@code utxoPool}. This should make a copy of
	 * utxoPool by using the UTXOPool(UTXOPool uPool) constructor.
	 */
	public TxHandler(UTXOPool utxoPool) {
		_utxoPool = new UTXOPool(utxoPool);
	}

	/**
	 * @return true if: (1) all outputs claimed by {@code tx} are in the current
	 *         UTXO pool, (2) the signatures on each input of {@code tx} are
	 *         valid, (3) no UTXO is claimed multiple times by {@code tx}, (4)
	 *         all of {@code tx}s output values are non-negative, and (5) the
	 *         sum of {@code tx}s input values is greater than or equal to the
	 *         sum of its output values; and false otherwise.
	 */
	public boolean isValidTx(Transaction tx) {

		try {

			TransactionEx txEx = new TransactionEx(tx, _utxoPool);
			return (txEx.getState() == TxState.VALID);

		} catch (Exception ex) {
			return false;
		}
	}

	/**
	 * Handles each epoch by receiving an unordered array of proposed
	 * transactions, checking each transaction for correctness, returning a
	 * mutually valid array of accepted transactions, and updating the current
	 * UTXO pool as appropriate.
	 */
	public Transaction[] handleTxs(Transaction[] possibleTxs) {

		Transaction[] mutuallyValidTransactions = null;
		// txMap will be initialized with valid OR TBD (state undecided)
		// transactions
		HashMap<String, TransactionEx> txMap = new HashMap<String, TransactionEx>();

		// populate the hashtable
		for (Transaction tx : possibleTxs) {
			try {
				TransactionEx txEx = new TransactionEx(tx, _utxoPool);
				txMap.put(txEx.getTxHash(), txEx);
			} catch (Exception ex) {
				// caught an invalid transaction,
			}
		}

		HashSet<UTXO> claimedUTXOs = new HashSet<UTXO>();
		HashSet<Transaction> mutuallyValidTransactionSet = new HashSet<Transaction>();
		for (Map.Entry<String, TransactionEx> entry : txMap.entrySet()) {
			TransactionEx txEx = entry.getValue();
			switch (txEx.getState()) {
			case VALID:
				tryAddTxToEpoch(txEx, claimedUTXOs, mutuallyValidTransactionSet);
				txEx.setState(TxState.RESOLVED);
				break;

			case TBD:
				tryAddDependentTxToEpoch(txEx, claimedUTXOs, mutuallyValidTransactionSet, txMap);
				txEx.setState(TxState.RESOLVED);
				break;

			default:
				continue;
			}
		}
		
		mutuallyValidTransactions = mutuallyValidTransactionSet.toArray(new Transaction[0]);
		return mutuallyValidTransactions;

	}

	private boolean tryAddDependentTxToEpoch(TransactionEx txEx, HashSet<UTXO> claimedUTXOs,
			HashSet<Transaction> mutuallyValidTransactionSet, HashMap<String, TransactionEx> txMap) {

		// resolve all transactions on which this transaction depends
		for (UTXO utxo : txEx.getInputUTXOs()) {

			if (claimedUTXOs.contains(utxo)) {
				return false;
			}

			Transaction.Output claimedOp = _utxoPool.getTxOutput(utxo);
			if (claimedOp == null) {
				// check if the utxo refers to a Transaction in the current
				// epoch
				TransactionEx prevTx = txMap.get(byteArrayToString(utxo.getTxHash()));
				if (prevTx == null) {
					return false;
				}
				switch (prevTx.getState()) {
				case VALID:
					tryAddTxToEpoch(prevTx, claimedUTXOs, mutuallyValidTransactionSet);
					break;

				case TBD:
					tryAddDependentTxToEpoch(prevTx, claimedUTXOs, mutuallyValidTransactionSet, txMap);
					break;
					
				default:
					break;
				}

				prevTx.setState(TxState.RESOLVED);
				if (_utxoPool.getTxOutput(utxo) == null) {
					return false;
				}
			} //end if

		}// end for loop

		claimedUTXOs.addAll(txEx.getInputUTXOs());
		mutuallyValidTransactionSet.add(txEx._tx);
		// update the UTXO pool .. remove input UTXOs from the
		// pool
		for (UTXO spent : txEx.getInputUTXOs()) {
			this._utxoPool.removeUTXO(spent);
		}
		// update the UTXO pool .. add output UTXOs to the pool
		for (UTXO out : txEx.getOutputUTXOs()) {
			this._utxoPool.addUTXO(out, txEx._tx.getOutput(out.getIndex()));
		}

		return true;

	}

	private boolean tryAddTxToEpoch(TransactionEx txEx, HashSet<UTXO> claimedUTXOs,
			HashSet<Transaction> mutuallyValidTransactionSet) {

		if (txEx.getState() != TxState.VALID) {
			return false;
		}

		if (!tryClaimUTXO(claimedUTXOs, txEx._inputUTXOs)) {
			return false;
		}

		mutuallyValidTransactionSet.add(txEx._tx);
		// update the UTXO pool .. remove input UTXOs from the
		// pool
		for (UTXO spent : txEx.getInputUTXOs()) {
			this._utxoPool.removeUTXO(spent);
		}
		// update the UTXO pool .. add output UTXOs to the pool
		for (UTXO out : txEx.getOutputUTXOs()) {
			this._utxoPool.addUTXO(out, txEx._tx.getOutput(out.getIndex()));
		}

		return true;
	}

	private boolean tryClaimUTXO(HashSet<UTXO> claimedUTXOs, HashSet<UTXO> _inputUTXOs) {

		for (UTXO utxo : _inputUTXOs) {
			if (claimedUTXOs.contains(utxo)) {
				// already claimed .. cannot claim again
				return false;
			}
		}

		// none of the utxos have been claimed .. claim them now
		claimedUTXOs.addAll(_inputUTXOs);
		return true;
	}

	public static String byteArrayToString(byte[] baHash) {
		// convert tx.hash to hex string
		StringBuilder sbTxHash = new StringBuilder();
		for (Byte b : baHash) {
			sbTxHash.append(String.format("%02x", b));
		}

		return sbTxHash.toString();
	}
	
	public enum TxState {
		VALID, INVALID, TBD, RESOLVED
	}

	public class TransactionEx {
		private Transaction _tx = null;
		private String _strTxHash;
		private byte[] _baTxHash = null;

		private UTXOPool _utxoPool;

		private HashSet<UTXO> _inputUTXOs = null;
		private double _totalInputValue = -1;

		private HashSet<UTXO> _outputUTXOs = null;
		private double _totalOutputValue = -1;

		private TxState _state = TxState.TBD;

		// constructor: throws exception for invalid transactions
		public TransactionEx(Transaction tx, UTXOPool utxoPool) throws Exception {
			_tx = tx;
			_utxoPool = utxoPool;
			init();
		}

		private void init() throws Exception {

			if (this._tx == null || this._utxoPool == null) {
				this.setState(TxState.INVALID);
				throw new Exception("Either Transaction is null or UTXO Pool is empty");
			}
			
			this.setTxHash();
			this.setState(TxState.VALID);

			if ((_totalOutputValue = getTotalOutputValue()) != -1) {
				this.setOutputUTXOs();
			} else {
				this.setState(TxState.INVALID);
				throw new Exception("Invalid Output values");
			}
			;

			if ((_totalInputValue = this.getTotalInputValue()) == -1) {
				this.setInputUTXOs(null);
				this.setState(TxState.INVALID);
				throw new Exception("Invalid Input values");
			}

			if (_totalInputValue < _totalOutputValue) {
				this.setState(TxState.INVALID);
				throw new Exception("Sum of Input values is less that sum of output values");
			}
		}

		public double getTotalOutputValue() {

			double totalOutputAmount = 0;
			for (Transaction.Output op : _tx.getOutputs()) {
				if (op.value < 0) {
					// check if the outputs are non negative
					return -1;
				}

				totalOutputAmount += op.value;
			}
			return totalOutputAmount;

		}

		private double getTotalInputValue() {
			double totalInputAmount = 0;
			_inputUTXOs = new HashSet<UTXO>(_tx.numInputs());

			for (int i = 0; i < _tx.numInputs(); i++) {
				Transaction.Input input = _tx.getInput(i);
				UTXO utxo = new UTXO(input.prevTxHash, input.outputIndex);

				// if adding the utxo set returns false, it means the utxo is
				// already claimed - double spend attack, .. so mark the
				// transaction invalid
				if (_inputUTXOs.add(utxo) == false) {
					return -1;
				}

				Transaction.Output claimedOp = _utxoPool.getTxOutput(utxo);
				if (claimedOp == null) {
					// the corresponding output which this input claims is not
					// in UTXO pool, probably its already spent or it may be
					// output
					// of a transaction in the current transaction set . We will
					// find out later... for now continue to next input
					this.setState(TxState.TBD);
					continue;
				}

				if (claimedOp.value < 0) {
					return -1;
				}

				// verify if the input signature is valid
				if (!Crypto.verifySignature(claimedOp.address, _tx.getRawDataToSign(i), input.signature)) {
					return -1;
				}

				totalInputAmount += claimedOp.value;
			}

			return totalInputAmount;
		}

		public TxState getState() {
			return _state;
		}

		public void setState(TxState _state) {
			this._state = _state;
		}

		public HashSet<UTXO> getOutputUTXOs() {
			return _outputUTXOs;
		}

		private void setOutputUTXOs() {
			// create output UTXOs
			this._outputUTXOs = new HashSet<UTXO>(_tx.numOutputs());
			for (int i = 0; i < _tx.numOutputs(); i++) {
				_outputUTXOs.add(new UTXO(this._baTxHash, i));
			}
		}

		public HashSet<UTXO> getInputUTXOs() {
			return _inputUTXOs;
		}

		private void setInputUTXOs(HashSet<UTXO> inputs) {
			this._inputUTXOs = inputs;
		}

		public String getTxHash() {
			return _strTxHash;
		}

		private void setTxHash() {
			try {
	            MessageDigest md = MessageDigest.getInstance("SHA-256");
	            md.update(_tx.getRawTx());
	            this._baTxHash = md.digest();
	        } catch (NoSuchAlgorithmException x) {
	        	this._baTxHash = _tx.getHash();
	            x.printStackTrace(System.err);
	        }
			
			this._strTxHash = byteArrayToString(this._baTxHash);
		}

	} // end class TransactionEx
}
